#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2020 drad <drader@adercon.com>

import datetime
import os
import psutil
import sys
import time

loops = int(os.environ.get("LOOP_AMOUNT", "300"))
pause = int(os.environ.get("PAUSE_AMOUNT", "2"))
output_dir = os.path.expandvars(os.environ.get("OUTPUT_DIR", "$HOME/.pymore/logs"))

cpu_sum = 0
load_sum = 0
memory_sum = 0
about = {"name": "pymore", "version": "1.8.0", "modified": "2020-02-25"}
rundts = datetime.datetime.now().strftime("%Y-%m-%d_%H%M%S")
log_base = f"{output_dir}/{rundts}_{about['name']}"
print(f"{about['name']} - v.{about['version']} ({about['modified']})")


def cpu_to_csv(cpu):
    return f"{str(cpu)}"


def load_to_csv(load):
    return f"{load[0]},{load[1]},{load[2]}"


def memory_to_csv(memory):
    return f"{memory.total},{memory.available},{memory.percent},{memory.used},{memory.free},{memory.active},{memory.inactive},{memory.buffers},{memory.cached},{memory.shared},{memory.slab}"


if __name__ == "__main__":
    print(f"Run: {loops * pause} (pause={pause})")
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    # open output files
    cpu_file = open(f"{log_base}_cpu.csv", "x")
    load_file = open(f"{log_base}_load.csv", "x")
    memory_file = open(f"{log_base}_memory.csv", "x")
    # write headers for each file.
    cpu_file.write("utilization_percent\n")
    load_file.write("one_minute,five_minute,fifteen_minute\n")
    memory_file.write(
        "total,available,percent,used,free,active,inactive,buffers,cached,shared,slab\n"
    )

    for i in range(loops):
        cpu = psutil.cpu_percent(interval=None)
        load = os.getloadavg()
        memory = psutil.virtual_memory()
        # throw away first result due to skew and cpu measure being 0.
        if i > 0:
            cpu_sum += cpu
            cpu_file.write(cpu_to_csv(cpu) + "\n")
            load_sum += load[0]
            load_file.write(load_to_csv(load) + "\n")
            memory_sum += memory.percent
            memory_file.write(memory_to_csv(memory) + "\n")
        # simple progress bar.
        j = (i + 1) / loops
        sys.stdout.write("\r[%-20s] %d%%" % ("=" * int(20 * j), 100 * j))
        sys.stdout.flush()
        time.sleep(pause)
    cpu_file.close()
    load_file.close()
    memory_file.close()
    print("\nResult (averages shown)")
    print(f" - CPU Usage:       {cpu_sum / (loops - 1)}")
    print(f" - Load (1 Minute): {load_sum / (loops - 1)}")
    print(f" - Memory (% Free): {100 - memory_sum / (loops - 1)}")
    print(f"Logs are located at: {log_base}")
