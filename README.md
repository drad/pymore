# README

Python Monitor Resources (pymore) is a lightweight, simple system resource monitor which, upon execution, runs in a loop, polling system resources at a given interval. Each poll result is stored and used to calculate the final result which consists of the average for each resource monitored. All resource metrics collected are wrote to a set of timestamped log files.

Monitored resources:
- cpu: cpu percent used (user)
- load: system load average
- memory: virtual memory used

This application is intended to be ran on the server which is being load tested to gather metrics of the server as load is underway. As such, this application was designed to consume as few resources (itself) as possible so as to not skew resource utilization by the load test.

Resource utilization of this application itself may vary based on the enviornment/architecture it is ran o. The following table shows resource utilization of this application along with other \*more variants:


| Application                              | Binary Size | VIRT   | RES    | SHR   | CPU% |
| ---------------------------------------- | :---------: | :----: | :----: | :---: | :--: |
| [pymore](https://gitlab.com/drad/pymore) | n/a         | 19,992 | 11,628 | 6,028 | 0.0  |
| [rumore](https://gitlab.com/drad/rumore) | 1.8M        | 2,928  | 888    | 768   | 0.0  |


## Setup
- (optional) create a /usr/bin symlink: `ln -sf -T /opt/apps/pymore/pymore.py /usr/bin/pymore`
  - note: adjust /opt/apps/pymore with the path to pymore.py
  - note: if you use create the above symlink, you can then run pymore by simply typing `pymore`


## Run
- normal run `./pymore.py`
- run with 20 loops and 2 second pauses (yields 40s runtime): `LOOP_AMOUNT=20 PAUSE_AMOUNT=2 ./pymore.py`
- normal 10m run: `LOOP_AMOUNT=300 PAUSE_AMOUNT=2 ./pymore.py`

## Example Output
Example output running with 30 loops, 2 second pause (60s run):
```
pymore - v.1.7.1 (2019-03-28)
Run: 60 (pause=2)
[====================] 100%
Result (averages shown)
 - CPU Usage:       1.489655172413793
 - Load (1 Minute): 0.15482758620689652
 - Memory (% Free): 61.4
Logs are located at: /home/drad/.pymore/logs/2019-04-24_120754_pymore
```


## Notices
- the first sample of the main loop is discarded as cpu stats are not applicable (iteration 0 cpu status =0) and to avoid startup skew


### Linting
- this app uses flake8: a multi-linter - it is a wrapper around the PyFlakes, pycodestyle, and Ned Batchelder's McCabe scripts and does a little more
  - connect to container
  - install flake8: `pip install flake8`
  - run flake8: `flake8 /opt/app`
    - NOTE: see the `.flake8` config file for ignored items and config of flake8

### Security
- this app uses bandit: a tool designed to find common security issues in Python code
  - connect to container
  - install bandit: `pip install bandit`
  - run bandit: `bandit -r /opt/app`

### Style
- this app uses black: The Uncompromising Code Formatter
  - black is installed as a pre-commit hook, see [Version control integration](https://pypi.org/project/black/#version-control-integration) for more information


## Memory
You should rely on available [1] (memory free).


## Links
* [psutils](https://psutil.readthedocs.io/en/latest/) - info on cpu and memory
* [os.getloadavg](https://docs.python.org/3/library/os.html#os.getloadavg) - info on load